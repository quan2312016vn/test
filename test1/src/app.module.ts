import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MulterModule } from "@nestjs/platform-express";
import { multerOptions } from './multer-storage/memory.storage';
import { CloudinaryModule } from './cloudinary/cloudinary.module';
import * as dotenv from "dotenv";

dotenv.config();


@Module({
  imports: [
    MulterModule.register(multerOptions),
    CloudinaryModule.register({
      cloud_name: process.env.CLOUDINARY_NAME || "",
      api_key: process.env.CLOUDINARY_API_KEY || "",
      api_secret: process.env.CLOUDINARY_API_SECRET || ""
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
