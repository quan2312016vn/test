import { Injectable } from '@nestjs/common';
import { CloudinaryService } from './cloudinary/cloudinary.service';
import * as imageDataUri from "image-data-uri";

@Injectable()
export class AppService {
  constructor(
    private cloudinaryService: CloudinaryService
  ) {}
  getHello(): string {
    return 'Hello World!';
  }

  async uploadFile(file: Express.Multer.File): Promise<void> {
    const dataURI = imageDataUri.encode(file.buffer, file.mimetype);
    await this.cloudinaryService.upload(dataURI, {
      folder: "test1"
    });
  }
}
