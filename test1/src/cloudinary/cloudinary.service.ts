import { Inject, Injectable } from '@nestjs/common';
import { CloudinaryOptions } from './types';
import { v2 as cloudinary } from "cloudinary";
import { promisify } from "util";

const uploadPromise = promisify(cloudinary.uploader.upload.bind(cloudinary.uploader));

@Injectable()
export class CloudinaryService {
    constructor(
        @Inject("CLOUDINARY_OPTIONS") options: CloudinaryOptions
    ) {
        cloudinary.config(options);
    }

    async upload(file: any, options: any = {}): Promise<void> {
        try {
            await uploadPromise(file, options);
        }
        catch(err) {
            throw err;
        }
    }
}
