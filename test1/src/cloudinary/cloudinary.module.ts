import { Module, DynamicModule } from '@nestjs/common';
import { CloudinaryService } from './cloudinary.service';
import { CloudinaryOptions } from "./types";


@Module({})
export class CloudinaryModule {
  static register(options: CloudinaryOptions): DynamicModule {
    return {
      module: CloudinaryModule,
      providers: [
        {
          provide: "CLOUDINARY_OPTIONS",
          useValue: options
        },
        CloudinaryService
      ],
      exports: [CloudinaryService]
    }
  }
}
