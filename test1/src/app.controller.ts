import { Controller, Get, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { AppService } from './app.service';
import * as fs from "fs/promises";
import * as path from "path";
import { CloudinaryModule } from "./cloudinary/cloudinary.module";


@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getHello(): Promise<String> {
    const content = await fs.readFile(path.resolve(__dirname, "../public/index.html"));
    return content.toString();
  }

  @Post("upload")
  @UseInterceptors(FileInterceptor("file"))
  async uploadFile(@UploadedFile() file: Express.Multer.File): Promise<string> {
    await this.appService.uploadFile(file);
    return "Done";
  }
}
